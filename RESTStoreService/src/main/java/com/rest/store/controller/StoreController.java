package com.rest.store.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.rest.store.model.Store;
import com.rest.store.service.StoreService;

/**
 * 
 * @author sbadgujar
 *
 */
@RestController
public class StoreController {

    private static final String GEO_CODE_SERVER = "http://maps.googleapis.com/maps/api/geocode/json?";

	@Autowired
	StoreService storeService; // Service which will do all data
								// retrieval/manipulation work

	// -----------Retrieve All Stores-----------------------------

	@RequestMapping(value = "/store/", method = RequestMethod.GET)
	public ResponseEntity<List<Store>> listAllStores() {
		System.out.println("Retrieving All Stores");
		List<Store> stores = storeService.findAllStores();
		if (stores.isEmpty()) {
			return new ResponseEntity<List<Store>>(HttpStatus.NO_CONTENT);// HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Store>>(stores, HttpStatus.OK);
	}

	// ---------Retrieve Single Store-------------------------------
	
	@RequestMapping(value = "/store/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Store> getStore(@PathVariable("id") long id) {
		System.out.println("Retieving Store with id " + id);
		Store store = storeService.findById(id);
		if (store == null) {
			System.out.println("Store with id " + id + " not found");
			return new ResponseEntity<Store>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Store>(store, HttpStatus.OK);
	}

	// -----------Create a Store---------------------------------

	@RequestMapping(value = "/store/", method = RequestMethod.POST)
	public ResponseEntity<Void> createStore(@RequestBody Store store,
			UriComponentsBuilder ucBuilder) {
		System.out.println("Creating Store " + store.getName());

		if (storeService.isStoreExist(store)) {
			System.out.println("A Store with name " + store.getName()
					+ " already exist");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		storeService.saveStore(store);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/store/{id}")
				.buildAndExpand(store.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	// ----------Update a Store----------------------------------------

	@RequestMapping(value = "/store/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Store> updateStore(@PathVariable("id") long id,
			@RequestBody Store store) {
		System.out.println("Updating Store " + id);

		Store currentStore = storeService.findById(id);

		if (currentStore == null) {
			System.out.println(">>updateStore : Store with id " + id + " not found");
			return new ResponseEntity<Store>(HttpStatus.NOT_FOUND);
		}else{
			System.out.println(">>updateStore : Store with id "+ id +" found");
		}

		if(null!=store.getName())	currentStore.setName(store.getName());
		if(null!=store.getAddress())	currentStore.setAddress(store.getAddress());
		if(null!=store.getCity())	currentStore.setCity(store.getCity());
		if(null!=store.getState())	currentStore.setState(store.getState());
		if(null!=store.getCountry())	currentStore.setCountry(store.getCountry());
		if(null!=store.getPincode())	currentStore.setPincode(store.getPincode());
		if(null!=store.getPhone())	currentStore.setPhone(store.getPhone());

		storeService.updateStore(currentStore);
		return new ResponseEntity<Store>(currentStore, HttpStatus.OK);
	}

	// ---------Delete a Store---------------------------------------

	@RequestMapping(value = "/store/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Store> deleteStore(@PathVariable("id") long id) {
		System.out.println("Fetching & Deleting Store with id " + id);

		Store store = storeService.findById(id);
		if (store == null) {
			System.out.println(">>deleteStore : Store with id " + id+ "not found");
			return new ResponseEntity<Store>(HttpStatus.NOT_FOUND);
		}else{
			System.out.println(">>deleteStore : Store with id " + id+ "found");
		}

		storeService.deleteStoreById(id);
		return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Delete All Stores------------------------------

	@RequestMapping(value = "/store/", method = RequestMethod.DELETE)
	public ResponseEntity<Store> deleteAllStores() {
		System.out.println("Deleting All Stores");

		storeService.deleteAllStores();
		return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/store/findAllStoresWithinPincode/{miles}/{pincode}", method = RequestMethod.GET)
	public ResponseEntity<List<Store>> findAllStoresWithinPincode(@PathVariable("miles") long miles, 
			@PathVariable("pincode") String pincode) {
		
		System.out.println("Retrieving All Stores which are within "+miles+" miles from the pincode : "+pincode);
		String response1 = getLocation(pincode);
        String[] result1 = parseLocation(response1);
        Double lat1=Double.valueOf(result1[0]);
        Double lng1=Double.valueOf(result1[1]);
        System.out.println("Latitude of input zipcode : " + lat1+" Longitude of input zipcode : " + lng1);
        
		List<Store> stores = storeService.findAllStores();
		List<Store> actualStoresList = new ArrayList<Store>();
		if (stores.isEmpty()) {
			return new ResponseEntity<List<Store>>(HttpStatus.NO_CONTENT);
		}
		
		for (Store store : stores) {
	        String response2 = getLocation(store.getPincode());
	        String[] result = parseLocation(response2);
	        Double lat2=Double.valueOf(result[0]);
	        Double lng2=Double.valueOf(result[1]);
	        System.out.println("Latitude of present zipcode : " + lat2+" Longitude of present zipcode : " + lng2);
	        
	        Double distance=haversine(lat1, lng1, lat2, lng2);
	       // System.out.println("Distance between 2 zipcodes in kms : "+distance);
	        System.out.println("Distance between 2 zipcodes in miles : "+distance * 0.621371);
	        distance =distance * 0.621371;
	       /* System.out.println("Enter the miles ");
	        Double inputDistance = Double.valueOf(scanner.nextLine());*/
	    	if(miles>distance){
	    		System.out.println("The store found within "+miles+" miles of input pincode : "+pincode);
	    		actualStoresList.add(store);
	    	}else{
	    		System.out.println("No store found within "+miles+" miles of input zipcode : "+pincode+" .So, Please exceed the range on distance in miles");
	    	}
		}
		System.out.println("Total number of stores found within "+miles+" miles of input zipcode "+pincode +" = "+actualStoresList.size());
		return new ResponseEntity<List<Store>>(actualStoresList, HttpStatus.OK);
	}
	
	private static String getLocation(String code)
    {
        String address = buildUrl(code);

        String content = null;

        try
        {
            URL url = new URL(address);

            InputStream stream = url.openStream();

            try
            {
                int available = stream.available();

                byte[] bytes = new byte[available];

                stream.read(bytes);

                content = new String(bytes);
            }
            finally
            {
                stream.close();
            }

            return (String) content.toString();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

	
    private static String buildUrl(String code)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(GEO_CODE_SERVER);
        builder.append("address=");
        builder.append(code.replaceAll(" ", "+"));
        builder.append("&sensor=false");
        return builder.toString();
    }

    private static String[] parseLocation(String response)
    {
        // Look for location using brute force.
        // There are much nicer ways to do this, e.g. with Google's JSON library: Gson
        //     https://sites.google.com/site/gson/gson-user-guide

        String[] lines = response.split("\n");

        String lat = null;
        String lng = null;

        for (int i = 0; i < lines.length; i++)
        {
            if ("\"location\" : {".equals(lines[i].trim()))
            {
                lat = getOrdinate(lines[i+1]);
                lng = getOrdinate(lines[i+2]);
                break;
            }
        }

        return new String[] {lat, lng};
    }

    private static String getOrdinate(String s)
    {
        String[] split = s.trim().split(" ");

        if (split.length < 1)
        {
            return null;
        }

        String ord = split[split.length - 1];

        if (ord.endsWith(","))
        {
            ord = ord.substring(0, ord.length() - 1);
        }

        // Check that the result is a valid double
        Double.parseDouble(ord);

        return ord;
    }
    
    public static double haversine(
            double lat1, double lng1, double lat2, double lng2) {
        int r = 6371; // average radius of the earth in km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
           Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) 
          * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = r * c;
        return d;
    }
}
