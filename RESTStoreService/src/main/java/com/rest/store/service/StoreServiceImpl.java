package com.rest.store.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rest.store.model.Store;

/**
 * 
 * @author sbadgujar
 *
 */
@Service("storeService")
@Transactional
public class StoreServiceImpl implements StoreService{

	
	private static final AtomicLong counter = new AtomicLong();
	
	private static List<Store> stores;
	
	static{
		stores= populateDummyStores();
	}

	public List<Store> findAllStores() {
		return stores;
	}
	
	public Store findById(long id) {
		for(Store store : stores){
			if(store.getId() == id){
				return store;
			}
		}
		return null;
	}
	
	public Store findByName(String name) {
		for(Store store : stores){
			if(store.getName().equalsIgnoreCase(name)){
				return store;
			}
		}
		return null;
	}
	
	public void saveStore(Store store) {
		store.setId(counter.incrementAndGet());
		stores.add(store);
	}

	public void updateStore(Store store) {
		int index = stores.indexOf(store);
		stores.set(index, store);
	}

	public void deleteStoreById(long id) {
		
		for (Iterator<Store> iterator = stores.iterator(); iterator.hasNext(); ) {
		    Store store = iterator.next();
		    if (store.getId() == id) {
		        iterator.remove();
		    }
		}
	}

	public boolean isStoreExist(Store store) {
		return findByName(store.getName())!=null;
	}
	
	public void deleteAllStores(){
		stores.clear();
	}

	private static List<Store> populateDummyStores(){
		List<Store> stores = new ArrayList<Store>();
		
		stores.add(new Store(counter.incrementAndGet(),"Domino's Pizza","1650 US-60","Dexter","MO","United States","63841","+1 573-624-8242"));
		stores.add(new Store(counter.incrementAndGet(),"McDonald's Restaurant #30707","949 WEST GRASSLAND DRIVE","American Fork","UT","United States","84003","(801)492-0260"));
		/*stores.add(new Store(counter.incrementAndGet(),"Jerome",45, 30000));
		stores.add(new Store(counter.incrementAndGet(),"Silvia",50, 40000));
		*/return stores;
	}


}
