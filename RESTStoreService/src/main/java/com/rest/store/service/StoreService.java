package com.rest.store.service;

import java.util.List;

import com.rest.store.model.Store;

/**
 * 
 * @author sbadgujar
 *
 */
public interface StoreService {

	Store findById(long id);
	
	Store findByName(String name);
	
	void saveStore(Store Store);
	
	void updateStore(Store Store);
	
	void deleteStoreById(long id);

	List<Store> findAllStores(); 
	
	void deleteAllStores();
	
	public boolean isStoreExist(Store Store);
}
