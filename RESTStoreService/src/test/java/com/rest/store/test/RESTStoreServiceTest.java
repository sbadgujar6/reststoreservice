package com.rest.store.test;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.web.client.RestTemplate;
import com.rest.store.model.Store;
 
public class RESTStoreServiceTest {
 
    public static final String REST_SERVICE_URI = "http://localhost:8082/RESTStoreService";
     
    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllStores(){
        System.out.println("Testing listAllStores API-----------");
         
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> storesMap = restTemplate.getForObject(REST_SERVICE_URI+"/store/", List.class);
         
        if(storesMap!=null){
            for(LinkedHashMap<String, Object> map : storesMap){
                System.out.println("Store : id="+map.get("id")+", Name="+map.get("name")+", Address="+map.get("address")+", city="+map.get("city")
                		+", state="+map.get("state")+", country="+map.get("country")+", pincode="+map.get("pincode")+", phone="+map.get("phone"));
            }
        }else{
            System.out.println("No store exist----------");
        }
    }
     
    /* GET */
    private static void getStore(){
        System.out.println("Testing getStore API----------");
        RestTemplate restTemplate = new RestTemplate();
        Store store = restTemplate.getForObject(REST_SERVICE_URI+"/store/1", Store.class);
        System.out.println(store);
    }
     
    /* POST */
    private static void createStore() {
        System.out.println("Testing create Store API----------");
        RestTemplate restTemplate = new RestTemplate();
        Store store = new Store(0L,"McDonald's Restaurant #5231","406 E STATE","American Fork","UT","United States","84003-2559","(801)756-5311");
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/store/", store, Store.class);
        System.out.println("Location : "+uri.toASCIIString());
    }
 
    /* PUT */
    private static void updateStore() {
        System.out.println("Testing update Store API----------");
        RestTemplate restTemplate = new RestTemplate();
        Store store  = new Store(1L,"Domino's Pizza","1650 US-60","Dexter","MO","United States","63841","+1 573-624-8242");
        restTemplate.put(REST_SERVICE_URI+"/store/1", store);
        System.out.println(store);
    }
 
    /* DELETE */
    private static void deleteStore() {
        System.out.println("Testing delete Store API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/store/3");
    }
 
 
    /* DELETE */
    private static void deleteAllStores() {
        System.out.println("Testing all delete Stores API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/store/");
    }
 
    public static void main(String args[]){
        listAllStores();
        getStore();
        createStore();
        listAllStores();
        updateStore();
        listAllStores();
        deleteStore();
        listAllStores();
        deleteAllStores();
        listAllStores();
    }
}